import React from 'react'
import Header from './components/Header'
import UserListPage from './pages/Users/UserListPage'
import UserCrearPage from './pages/Users/UserCrearPage'
import UserEditarPage from './pages/Users/UserEditarPage'
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom'
const App = () => {
  return (
    <>
      <BrowserRouter>
        <Header />

        <Switch>
          <Route path="/usuarios/editar/:id">
            <UserEditarPage />
          </Route>
          <Route path="/usuarios/crear">
            <UserCrearPage />
          </Route>
          <Route path="/usuarios">
            <UserListPage />
          </Route>
        </Switch>

      </BrowserRouter>
    </>

  )
}

export default App
